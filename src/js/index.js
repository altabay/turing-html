import "bootstrap";
import { library, dom } from "@fortawesome/fontawesome-svg-core";
import {
  faShoppingBag,
  faChevronRight,
  faSearch,
  faTimes,
  faCircle
} from "@fortawesome/free-solid-svg-icons";
import { faHeart, faEnvelope } from "@fortawesome/free-regular-svg-icons";
import {
  faInstagram,
  faPinterest,
  faTwitter,
  faFacebookF
} from "@fortawesome/free-brands-svg-icons";

library.add(
  faEnvelope,
  faShoppingBag,
  faChevronRight,
  faSearch,
  faHeart,
  faInstagram,
  faPinterest,
  faTwitter,
  faFacebookF,
  faTimes,
  faCircle
);
dom.watch();

$(document).ready(function() {
  $(".add-to-cart").on("click", function() {

    let cart = $(".cart");
    let imgtodrag = $(this).parents(".product-card").find("img").eq(0);
    let indicator = $('.top-line-badge');
    let price = $('.top-line-price');
    if (imgtodrag) {
      let imgclone = imgtodrag.clone()
        .offset({
          top: imgtodrag.offset().top,
          left: imgtodrag.offset().left
        })
        .css({
          "opacity": "0.5",
          "position": "absolute",
          "height": "150px",
          "width": "150px",
          "z-index": "100"
        })
        .appendTo($("body"))
        .animate({
          "top": cart.offset().top + 10,
          "left": cart.offset().left + 10,
          "width": 75,
          "height": 75
        }, 1000);

      setTimeout(function() {
        let newInfo = parseInt(indicator.html()) + 1;
        let newPrice = parseInt(price.html()) + 14.99;
        indicator.html(newInfo);
        price.html(newPrice.toFixed(2));
      }, 1500);

      imgclone.animate({
        "width": 0,
        "height": 0
      }, function() {
        $(this).detach();
      });
    }
  });
});